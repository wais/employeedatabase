//
//  main.m
//  employeedatabase
//
//  Created by Wais Al Korni on 5/23/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
