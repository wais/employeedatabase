//
//  AppDelegate.h
//  employeedatabase
//
//  Created by Wais Al Korni on 5/23/17.
//  Copyright © 2017 Ice House. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

